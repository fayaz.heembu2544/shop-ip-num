import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/', //setpath bpage
    name: 'home', //set namespace
    component: HomeView //call file import
  },
  {
    path: '/toolbar',
    name: 'toolbar',
    component: () => import(/* webpackChunkName: "about" */ '../views/Toolbar.vue'),
    children:[
      {
        path: '/about',
        name: 'about',
        component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
      },
      // {
      //   path: '/me',
      //   name: 'me',
      //   component: () => import(/* webpackChunkName: "about" */ '../views/Me.vue')
      // },
      {
        path: '/login',
        name: 'login',
        component: () => import(/* webpackChunkName: "about" */ '../views/Login.vue')
      },
      {
        path: '/simple',
        name: 'simple',
        component: () => import(/* webpackChunkName: "about" */ '../views/Simple.vue')
      },
      {
        path: '/apicon',
        name: 'apicon',
        component: () => import(/* webpackChunkName: "about" */ '../views/Apicon.vue')
      },
    ]
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
