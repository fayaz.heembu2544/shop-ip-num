import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/', //setpath bpage
    name: 'home', //set namespace
    component: HomeView //call file import
  },
  {
    path: '/toolbar',
    name: 'toolbar',
    component: () => import(/* webpackChunkName: "about" */ '../views/Toolbar.vue'),
    children:[
      {
        path: '/about',
        name: 'about',
        component: () => import(/* webpackChunkName: "about" */ '../views/AboutView.vue')
      },

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
